'use strict';
var selectedProgressBar;
var testArray;
var containerLimit;
var dynamicVal;

function loadData(){
    containerLimit = testArray.limit;

    var progString='';
    for(var i=0; i< testArray.bars.length; i++){
        dynamicVal = (testArray.bars[i] * 100)/containerLimit;
        progString=progString+'<div id="p'+i+'" class="progressBar" style="width: '+dynamicVal+'%;background-color: dodgerblue;">'+testArray.bars[i]+'%</div>';
    }
    document.getElementById("bars").innerHTML = progString;

    var btnString='';
    for(var j=0;j<testArray.buttons.length;j++){
        btnString=btnString+ '<button onclick="changeWidth(this.value)" value="'+testArray.buttons[j]+'">'+testArray.buttons[j]+'</button>';   
    }
    document.getElementById("buttons").innerHTML = btnString;  

    var barsSelect = '<select onchange="onProgressSelect(this.value)">';
    var barOptions ='';
    for(var k=0;k<testArray.bars.length;k++){
        barOptions = barOptions+'<option value="p'+[k]+'">Progress Bar '+[k]+'</option>';
    }
    barsSelect = barsSelect + barOptions + '</select>';
    document.getElementById("barsSelect").innerHTML = barsSelect; 

    onProgressSelect("p0");
}
function postCallback(xhttp){
    if (xhttp.readyState==4 && xhttp.status==200){
        testArray = JSON.parse(xhttp.responseText);  
        document.getElementById("loaderFinish").innerHTML = '';
        loadData(); 
    } 
}

function onDomLoad(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange=function () { postCallback(xhttp); };
    xhttp.open("GET", "http://pb-api.herokuapp.com/bars", true);
    xhttp.send();
}



function onProgressSelect(sId){
    selectedProgressBar = document.getElementById(sId);
}

function changeWidth(btnVal){
    if(selectedProgressBar != undefined && selectedProgressBar != null){
        var dynamicWidth = parseInt(selectedProgressBar.innerHTML.substring(0,selectedProgressBar.innerHTML.length-1))
        var newWidth =  dynamicWidth + parseInt(btnVal);
        if(newWidth < 0){
            selectedProgressBar.style.background = 'dodgerblue';
            selectedProgressBar.style.width = "0%";
            selectedProgressBar.innerHTML = "0%";
        }else if (newWidth >= testArray.limit) {
            selectedProgressBar.style.background = 'red';
            selectedProgressBar.innerHTML = newWidth+"%";
            selectedProgressBar.style.width = "100%";
        }else{
            dynamicVal = (newWidth * 100)/containerLimit;
            selectedProgressBar.style.width = dynamicVal+"%";
            selectedProgressBar.innerHTML = newWidth+"%";
            selectedProgressBar.style.background = 'dodgerblue';
        }    
    }
}